const express = require('express');
const dbConnection = require('./config/db');
const dotenv = require('dotenv');
const app = express();
const authRoutes = require('./routes/authRoutes.js');
const cookieParser = require('cookie-parser');
const flash = require('connect-flash');
const expressSession = require('express-session');
const authMiddleware = require('./middleware/authMiddleware');
const jwt = require("jsonwebtoken");
const User = require("./models/User");


// Load config
dotenv.config({path:'./config/config.env'});

// Set the PORT
const PORT = process.env.PORT || 5000;

// Initialize a json parser
app.use(express.urlencoded({extended:false}));
app.use(express.json());

// Initialize cookie-parser
app.use(cookieParser());

// Static files
app.use(express.static('public'));

// Initialize flash area
app.use(flash())

app.use(expressSession({
    secret: 'keyboard cat'
}))

// Connect to MongoDB
dbConnection();

// View engine
app.set('view engine', 'ejs');

//routes
app.use('*', (req, res, next) => {
    const token = req.cookies.jwt;

    if (token) {
        jwt.verify(token, 'net ninja secret', async(err, decodedToken) =>{
            if (err){
                console.log(err.message);
                req.locals.user = null;
                next();
            }
            else{
                console.log(decodedToken);
                res.locals.user = await User.findById(decodedToken.id);
                next();
            }
        })
    }
    else {
        res.locals.user = null;
        next();
    }
});

app.get('/', authMiddleware, (req, res)=>{
    res.render('home');
})

app.get('/smoothies', authMiddleware, (req, res) => res.render('smoothies'));

app.use(authRoutes);

app.listen(PORT, ()=>console.log(`The server is listening on port ${PORT}`));
