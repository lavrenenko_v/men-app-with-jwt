const User = require('../models/User');
const jwt = require('jsonwebtoken');

//handle login data
const handleFormData = (req, field) => {
    let email = "";
    let password = "";
    const data = req.flash(field)[0];
    if(typeof data != "undefined"){
        email = data.email
        password = data.password
    }
    return {email:email, password:password};
}



//handle errors
const handleErrors = (err) => {
    let errors = {email:'', password:''};

    // duplicate error code
    if (err.code === 11000){
        errors.email = 'That email is already registered';
        return errors;
    }

    // incorrect email
    if (err.message === 'Emails do not match'){
        errors.email = 'Emails do not match'
    }

    // incorrect password
    if (err.message === 'Passwords do not match'){
        errors.password = 'Passwords do not match'
    }


    // validation errors
    if (err.message.includes('user validation failed')){
        Object.values(err.errors).forEach(({properties})=>{
            errors[properties.path] = properties.message;
        })
    }
    return errors;
}

const maxAge = 3 * 24 * 60 * 60;
const createToken = (id) => {
    return jwt.sign({id}, 'net ninja secret', {
        expiresIn: maxAge
    });
}



module.exports.signup_get = (req, res)=>{
    const {email, password} = handleFormData(req, 'data');
    const data = req.flash('validationErrors')[0];
    res.render('signup', {
        errors:data?Object.values(data):'',
        email:email,
        password:password
    });
}

module.exports.login_get = (req, res)=>{
    const {email, password} = handleFormData(req, 'loginData');
    const data = req.flash('loginError')[0];
    res.render('login', {
        errors:data?Object.values(data):'',
        email:email,
        password:password
    });

}

module.exports.signup_post = async(req, res)=>{
    try{
        const user = await User.create(req.body);
        const token = createToken(user.id);
        res.cookie('jwt', token, {
            httpOnly:true,
            maxAge: maxAge * 1000
        });
        res.status(201).redirect('/');
    }
    catch (err){
        const errors = handleErrors(err);
        req.flash('validationErrors', errors);
        req.flash('data', req.body);
        res.redirect('/signup');
    }
}

module.exports.login_post = async(req, res)=>{
   try{
       const user = await User.login(req.body);
       const token = createToken(user.id);
       res.cookie('jwt', token, {
           httpOnly:true,
           maxAge: maxAge * 1000
       });
       res.redirect('/')
   }
   catch(err){
       const errors = handleErrors(err);
       req.flash('loginError', errors);
       req.flash('loginData', req.body);
       res.redirect('/login');
   }

}

module.exports.logout_get = async(req, res) =>{
    res.cookie('jwt', '', {maxAge: 1});
    res.redirect('/login');
}
